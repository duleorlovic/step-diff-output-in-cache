# diff-output-in-cache

Create diff file for your output in cache dir. Generated file is copied to output dir so you can use it for deploy process. Deploy only files that differs from previous deploy.

Generated diff-file has modificatior (A, M, D) and file path on each line. For example:
```
A new_image.jpg
M index.html
D unused_image.jpg
```

# Options

* `output-dir` (optional) Directory in which you want to create diff-file. Default is $WERCKER_OUPUT_DIR which is input directory for deploy process.

# Example


```yaml
build:
  steps:
    - duleorlovic/diff-output-in-cache:
        output-dir: $WERCKER_OUTPUT_DIR
```

# License

The MIT License (MIT)

Copyright (c) 2014 duleorlovic

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Changelog


## 0.0.1

- Initial release
