if [ -n "$WERCKER_DIFF_OUPUT_IN_CACHE_OUTPUT_DIR" ] 
then 
  export OUTPUT_DIR=$WERCKER_DIFF_OUPUT_IN_CACHE_OUTPUT_DIR
  warning "Using $OUTPUT_DIR as code source. Code should be copied to \$WERCKER_OUPUT_DIR. diff-file will be copied there" 
else
  export OUPUT_DIR=$WERCKER_OUPUT_DIR
fi 

info "Calculation for diff file"

mkdir -p $WERCKER_CACHE_DIR/diff-output-in-cache
#ls $WERCKER_CACHE_DIR/diff-output-in-cache
cp -rv $WERCKER_OUTPUT_DIR/* $WERCKER_CACHE_DIR/diff-output-in-cache/

cd $WERCKER_CACHE_DIR/diff-output-in-cache
git init # in case git is not initialized
git add .
git diff --name-status --staged | tee diff-file
#git rm --cached diff-file
git config user.email "pleasemailus@wercker.com"
git config user.name "werckerbot"
git commit -m $WERCKER_GIT_BRANCH-$WERCKER_GIT_COMMIT
success "create diff-file in $WERCKER_CACHE_DIR/diff-output-in-cache/diff-file"
mv diff-file $WERCKER_OUTPUT_DIR
git rm -r . # for next time

warning "what if we do not deploy this changes. we should save until we deploy"

